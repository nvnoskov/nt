# NT timeparse

This MVP project aims to provide a  way to parse time-related data in various formats.


## Research on 3rd Party Libraries

As part of this project, extensive research has been conducted on various 3rd party libraries. Notably, the project leverages the following libraries:

- Duckling from Facebook: Duckling is a library that enables the extraction of structured information from text, including date and time expressions.

- Stanford CoreNLP's SUTime Java library: SUTime is a library for recognizing and normalizing time expressions in text.


To implement my own logic I need to have a set or regex rules to extract information about date and time
`ctparse` library is using same idea so I used it as a base. 

```
_regex_hour = r"(?:[01]?\d)|(?:2[0-3])"
_regex_minute = r"[0-5]\d"
_regex_day = r"[012]?[1-9]|10|20|30|31"
_regex_month = r"10|11|12|0?[1-9]"
_regex_year = r"(?:19\d\d)|(?:20[0-2]\d)|(?:\d\d)"
```

## CI/CD Configuration

In addition to the core functionality, this project also includes a CI/CD (Continuous Integration/Continuous Deployment) process. This process ensures that the code is thoroughly tested and a Docker image is built for easy deployment.

By automating the testing and deployment processes, the CI/CD pipeline helps maintain the quality and reliability of the project.

## Getting Started

To get started with this project, please refer to the following sections:

- Installation: Instructions on how to install and set up the project.

```bash
docker run -it --rm -p 8000:80 registry.gitlab.com/nvnoskov/nt:latest
```

Open URL using **text** and **base_date** http://127.0.0.1:8000/date?text=Tomorrow%20at%2011am&base_date=2023-02-24

Or you can make request on new terminal window
```bash
curl -XGET http://127.0.0.1:8000/date?text=Teuesdey%20oif%20Nexx%20weak%20att%209%20pm&base_date=2023-02-24
```

## Next Steps

- While a phonetic solution for spelling correction is fast, it's not always accurate. To improve this, I plan to use pre-tuned models for spelling correction or employ an n-gram algorithm to manually check words based on an English dictionary.
- The `ctparse` tool needs improvement. For instance, it fails to extract the time from phrases like 'Tuesday of next week at 9 pm', although it works correctly with 'at 9 pm Tuesday of next week'.