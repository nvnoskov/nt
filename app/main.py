from datetime import datetime
from typing import Union

from fastapi import FastAPI

from app.parse import get_datetime_from_text

app = FastAPI()


@app.get("/")
def read_root():
    return {"Hello": "World"}


@app.get("/date")
def read_item(text: str, base_date: Union[str, None] = None):
    if base_date is not None:
        reference_time = datetime.fromisoformat(base_date)
    else:
        reference_time = datetime.now()
    return {"text": text,
            "date": get_datetime_from_text(text, reference_time=reference_time)}
