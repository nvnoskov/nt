import jellyfish

time_related_words = [
    "at",
    "on",
    "in",
    "by",
    "before",
    "after",
    "past",
    "till",
    "until",
    "ago",
    "for",
    "of",
    "during",
    "within",
    "next",
    "last",
    "this",
    "every",
    "all",
    "any",
    "today",
    "yesterday",
    "tomorrow",
    "tonight",
    "morning",
    "noon",
    "afternoon",
    "evening",
    "night",
    "week",
    "month",
    "year",
    "hour",
    "monday",
    "tuesday",
    "wednesday",
    "thursday",
    "friday",
    "saturday",
    "sunday",
    "january",
    "february",
    "march",
    "april",
    "may",
    "june",
    "july",
    "august",
    "september",
    "october",
    "november",
    "december",
    "spring",
    "summer",
    "autumn",
    "winter",
    "previous",
    "first",
]

time_related_words_dict = {}
time_related_words_dict_nysiis = {}
time_related_words_dict_match_rating_codex = {}

for word in time_related_words:
    mtph = jellyfish.metaphone(word)
    if mtph not in time_related_words_dict:
        time_related_words_dict[mtph] = word
    else:
        print("Duplicate", word, time_related_words_dict[mtph])

    nysiis = jellyfish.nysiis(word)
    if nysiis not in time_related_words_dict_nysiis:
        time_related_words_dict_nysiis[nysiis] = word
    else:
        print("Duplicate nysiis", word, time_related_words_dict_nysiis[nysiis])

    mrc = jellyfish.match_rating_codex(word)
    if mrc not in time_related_words_dict_match_rating_codex:
        time_related_words_dict_match_rating_codex[mrc] = word
    else:
        print(
            "Duplicate match_rating_codex",
            word,
            time_related_words_dict_match_rating_codex[mrc],
        )


def fix_spelling(text):
    """
    Fixes spelling errors in the given text by replacing time-related words with their correct counterparts.

    Args:
        text (str): The input text to fix spelling errors in.

    Returns:
        str: The text with spelling errors fixed.
    """

    words = text.lower().split()
    for i, word in enumerate(words):
        mtph = jellyfish.metaphone(word)
        if mtph in time_related_words_dict:
            words[i] = time_related_words_dict[mtph]
        else:
            nysiis = jellyfish.nysiis(word)
            if nysiis in time_related_words_dict_nysiis:
                words[i] = time_related_words_dict_nysiis[nysiis]
            # need more time to test this, simplest solution is to calculate levenshtein distance for each word and replace if distance is 1 or use ngram algorytm to find the most similar word
            # else:
            #     if jellyfish.damerau_levenshtein_distance(word, "next") <= 1:
            #         words[i] = "next"

    return " ".join(words)
