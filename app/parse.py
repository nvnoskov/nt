from ctparse import ctparse
from datetime import datetime

from app.spelling import fix_spelling

# Set reference time
ts = datetime.now()


def get_datetime_from_text(input: str, reference_time: datetime = ts) -> datetime:
    """
    Converts a string representation of time to a datetime object.

    Args:
        input (str): The string representation of time.
        reference_time (datetime, optional): The reference time to use for parsing. Defaults to ts.

    Returns:
        datetime: The parsed datetime object.

    """
    t = ctparse(input, ts=reference_time)

    if not t or t.score < -1500:
        # Check spelling of input
        input = fix_spelling(input)
        t = ctparse(input, ts=reference_time)
    if not t or t.resolution.year is None:
        return None
    dt = datetime(t.resolution.year, t.resolution.month, t.resolution.day, t.resolution.hour or 0, t.resolution.minute or 0)
    return dt
