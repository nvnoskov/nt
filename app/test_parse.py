import unittest
from datetime import datetime

from app.parse import get_datetime_from_text

# Example:
# Assuming Reference date of = '2023-02-24'
# ‘Tomorrow at 11am' -> '2023-02-25 11:00’
# 'This coming Tuesday at 9pm' -> '2023-02-28 21:00'
# 'Next Tuesday at 9pm' -> '2023-02-28 21:00'
# Bonus case with spelling mistakes:
# 'Teuesdey oif Nexx weak att 9 pm' -> '2023-02-28 21:00'

cases = {
    "Next Monday at 4pm": "2023-03-06 16:00",
    "This Saturday at 9AM": "2023-02-25 09:00",
    "Tomorrow at 2pm": "2023-02-25 14:00",
    "Next Thursday at 5PM": "2023-03-09 17:00",
    "This coming Wednesday at 1pm": "2023-03-01 13:00",
    "Tomorrow at 6AM": "2023-02-25 06:00",
    "Next Tuesday at 12pm": "2023-03-07 12:00",
    "This Saturday at 7PM": "2023-02-25 19:00",
    "Tomorrow at 8am": "2023-02-25 08:00",
    "Next Sunday at 11AM": "2023-03-05 11:00",
    "This coming Monday at 10pm": "2023-02-27 22:00",
    "Tomorrow at 3PM": "2023-02-25 15:00",
    "Next Saturday at 2pm": "2023-03-04 14:00",
    "This coming Thursday at 9AM": "2023-03-02 09:00",
    "Tomorrow at 4pm": "2023-02-25 16:00",
    "Next Wednesday at 1PM": "2023-03-08 13:00",
    "This coming Tuesday at 11am": "2023-02-28 11:00",
    "Tomorrow at 5AM": "2023-02-25 05:00",
    "Next Monday at 12pm": "2023-03-06 12:00",
    "This Sunday at 7PM": "2023-02-26 19:00",
# Spelling mistakes
    "Nxt Monday at 4pm": "2023-03-06 16:00",
    "Tomorow at 2pm": "2023-02-25 14:00",
    "Next Thrsday at 5PM": "2023-03-09 17:00",
    "This coming Wednsday at 1pm": "2023-03-01 13:00",
    "Tomorow at 6AM": "2023-02-25 06:00",
    "Nexx Tuesday 12pm": "2023-03-07 12:00",
    "Tomorow at 8am": "2023-02-25 08:00",
    "Nexx Sunday at 11AM": "2023-03-05 11:00",
    "Ths comng Monday at 10pm": "2023-02-27 22:00",
    "Tomorow at 3PM": "2023-02-25 15:00",
    "Nexx Saturday at 2pm": "2023-03-04 14:00",
    "This coming Thrsday at 9AM": "2023-03-02 09:00",
    "Tomorow at 4pm": "2023-02-25 16:00",
    "Nexx Wednesday at 1PM": "2023-03-08 13:00",
    "Ths comng Tuesday at 11am": "2023-02-28 11:00",
    "Tomorow at 5AM": "2023-02-25 05:00",
    "Nexx Monday at 12pm": "2023-03-06 12:00",
    "Ths Sunday at 7PM": "2023-02-26 19:00"
}
class TestParse(unittest.TestCase):
    def setUp(self) -> None:
        self.ts = datetime(2023, 2, 24) #Friday
    def test_dates(self):
        t = get_datetime_from_text("May 5th 2:30 in the afternoon", reference_time=self.ts)
        self.assertEqual(t.year, self.ts.year)
        self.assertEqual(t.month, 5)
        self.assertEqual(t.day, 5)
        self.assertEqual(t.hour, 14)
        self.assertEqual(t.minute, 30)

    def test_tomorrow(self):
        t = get_datetime_from_text("Tomorrow at 11am", reference_time=self.ts)
        self.assertEqual(t.year, self.ts.year)
        self.assertEqual(t.month, self.ts.month)
        self.assertEqual(t.day, self.ts.day + 1)
        self.assertEqual(t.hour, 11)

    def test_this_coming_tuesday(self):
        t = get_datetime_from_text("This coming Tuesday at 9pm", reference_time=self.ts)
        self.assertEqual(t.year, self.ts.year)
        self.assertEqual(t.month, self.ts.month)
        self.assertEqual(t.day, self.ts.day + 4)
        self.assertEqual(t.hour, 21)

    def test_next_tuesday(self):
        t = get_datetime_from_text("Next Tuesday at 9pm", reference_time=self.ts)
        self.assertEqual(t.year, self.ts.year)
        self.assertEqual(t.month, 3)
        self.assertEqual(t.day, 7)
        self.assertEqual(t.hour, 21)   

    def test_spelling_mistakes(self):
        t = get_datetime_from_text(" att 9pm oif Teuesdey Nexx weak", reference_time=self.ts)
        self.assertEqual(t.year, self.ts.year)
        self.assertEqual(t.month, 3)
        self.assertEqual(t.day, 7)
        self.assertEqual(t.hour, 21)

    def test_cases(self):
        for case in cases:
            t = get_datetime_from_text(case, reference_time=self.ts)
            self.assertEqual(t.strftime("%Y-%m-%d %H:%M"), cases[case])
            print(case, t.strftime("%Y-%m-%d %H:%M"), cases[case])
 
if __name__ == '__main__':
    unittest.main()